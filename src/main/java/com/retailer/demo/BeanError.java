package com.retailer.demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
/**
 * 
 * @author Srinu D
 * Class BeanError
 *
 */
public class BeanError {
	
	private String status;
	private Map<String,String> errors=new HashMap<>();

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setStatus(HttpStatus status) {
		this.status = status.toString();
	}
	
	public String getStatus() {
		return status;
	}

}
