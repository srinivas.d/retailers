package com.retailer.demo;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
/**
 * 
 * @author Srinu D
 * class RetailerException
 */
public class RetailerException extends Exception{
	
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
	private BindingResult bindingResult;
	
	public RetailerException() {
		
	}

	public RetailerException(HttpStatus httpStatus, BindingResult bindingResult) {
		if(httpStatus!=null) {
		 this.httpStatus = httpStatus;
		}
		this.bindingResult = bindingResult;
	}
	
	public RetailerException(String message, HttpStatus code) {
		super(message);
		this.httpStatus = code;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public BindingResult getBindingResult() {
		return bindingResult;
	}

}
