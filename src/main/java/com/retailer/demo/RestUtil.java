package com.retailer.demo;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Srinu D
 * class RestUtil
 *
 */
public class RestUtil {
	public static Map map(Object... args) {
		Map map = new HashMap();
		for (int i = 0; i < args.length; i += 2) {
			map.put(args[i], args[i + 1]);
		}
		return map;
	}
	
	public static Map success(Object response) {
		return map("status","200","Error","no","response",response);
	}
	
	public static Map error(Object response) {
		return map("Error","yes","response",response);
	}

}
