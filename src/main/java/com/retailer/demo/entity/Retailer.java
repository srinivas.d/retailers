package com.retailer.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedBy;

import lombok.Data;

@Entity
@Table
@Data
public class Retailer {
	@Id
	@GeneratedValue
	private Integer id;
	
	@NotEmpty(message = "{NotEmpty.Retailer.shopName}")
	@Size(max = 75,message = "{Size.Retailer.shopName}")
	@Column(name = "shopName", nullable = false)
	private String shopName; 
	
	@NotEmpty(message = "{NotEmpty.Retailer.proprietorName}")
	@Size(max = 75,message = "{Size.Retailer.proprietorName}")
	@Column(name = "proprietorName", nullable = false)
	private String proprietorName;
	
	@NotEmpty(message = "{NotEmpty.Retailer.category}")
	@Size(max = 75,message = "{Size.Retailer.category}")
	@Column(name = "category", nullable = false)
	private String category;
	
	@NotEmpty(message = "{NotEmpty.Retailer.address}")
	@Size(max = 200,message = "{Size.Retailer.address}")
	@Column(name = "address", nullable = false)
	private String address;
	
	@Column(name = "latitude", nullable = false)
	private String latitude;
	@Column(name = "longitude", nullable = false)
	private String longitude;
	
	
	@Column(name = "created_date",  updatable = false)
	private Date createdDate = new Date();
	
	@Column(name = "last_modified_date")
	private Date lastModifiedDate = new Date();
	
	@CreatedBy
	@Column(name = "created_by", length = 50, updatable = false)
	private String createdBy;
	
	@Column(name = "last_modified_by", length = 50)
	private String lastModifiedBy;

	@Column(name = "createdIp",  length = 50, updatable = false)
	private String createdIp;
	
	@Column(name = "updatedIp",  length = 50)
	private String updatedIp;

	

	
	
}
