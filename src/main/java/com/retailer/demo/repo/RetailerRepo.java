package com.retailer.demo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.retailer.demo.entity.Retailer;

public interface RetailerRepo extends JpaRepository<Retailer, Integer>{
	
	/**
	 * this method is used for search by params
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	List<Retailer> findByLatitudeAndLongitude(@Param("latitude") String latitude,@Param("longitude") String longitude);

}

