
package com.retailer.demo;

import java.util.Map;

import org.springframework.http.HttpStatus;
/**
 * 
 * @author Srinu D
 * class NoAvailabilityException
 *
 */
public class NoAvailabilityException extends RuntimeException {
   
	private HttpStatus status;
	private Map<String,Object> response;
	
	public NoAvailabilityException(HttpStatus status,Map<String,Object> response) {
		this.status=status;
		this.response=response;
		this.response.put("status", status.toString());
	}

	public HttpStatus getStatus() {
		return status;
	}

	public Map<String, Object> getResponse() {
		return response;
	}
}
