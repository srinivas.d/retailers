package com.retailer.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Srinu D
 * class ManagerController
 */
@Controller
public class ManagerController {

	@RequestMapping("/retailer")
	public String getPage() {

		return "retailer";

	}
	
	@RequestMapping("/addRetailer")
	public String addShop() {

		return "addRetailer";

	}
	
	@RequestMapping("/searchRetailer")
	public String getSearch() {

		return "searchShop";

	}

}
