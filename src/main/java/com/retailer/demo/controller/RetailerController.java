package com.retailer.demo.controller;

import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.retailer.demo.NoAvailabilityException;
import com.retailer.demo.RestUtil;
import com.retailer.demo.RetailerException;
import com.retailer.demo.entity.Retailer;
import com.retailer.demo.repo.RetailerRepo;

import lombok.Synchronized;

/**
 * 
 * @author Srinu D
 * Class RetailerController
 *
 */
@RestController
@RequestMapping("/retailer")
public class RetailerController {
	
	private static final Logger LOG = LoggerFactory.getLogger(RetailerController.class);
	
	@Autowired
	RetailerRepo retailerRepo;
	
	/**
	 * this method is used for saving record
	 * @param retailer
	 * @param bindingResult
	 * @return
	 * @throws RetailerException
	 */
	@PostMapping("/create")
	@Transactional
	public @Synchronized List<Retailer> create(@Valid @RequestBody Retailer retailer,BindingResult bindingResult) throws RetailerException {
		if(bindingResult.hasErrors()) {
			LOG.error("got error while creating");
			throw new RetailerException(HttpStatus.BAD_REQUEST, bindingResult);
		}
		 retailerRepo.save(retailer);
		 LOG.info("recoed saved with name of..."+retailer.getShopName());
		return retailerRepo.findAll();
		
	}
	/**
	 * this method is used for search by params
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws RetailerException
	 */
	@PostMapping("/list")
	public @Synchronized List<Retailer> grid(@RequestParam(required = false) String latitude,@RequestParam(required = false) String longitude)  {
		if(Objects.nonNull(latitude) && Objects.nonNull(longitude)) {
			return retailerRepo.findByLatitudeAndLongitude(latitude,longitude);
		}else {
			LOG.error("got error while searching");
			throw new NoAvailabilityException(HttpStatus.BAD_REQUEST, RestUtil.error("Please Give latitude and longitude"));
			
		}
		
		
	}

}
