package com.retailer.demo;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;


/**
 * 
 * @author Srinu D
 * class ExceptionHandlerAdvice
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {
	

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<?> handleException(MethodArgumentTypeMismatchException exception) {
		BeanError beanError = new BeanError();
		Map<String, String> errors = beanError.getErrors();
		errors.put(exception.getName(),"please send correct format of "+exception.getRequiredType().getSimpleName()+ " type data "+exception.getValue());
		errors.put("Error Description",exception.getRootCause().getLocalizedMessage());
		return ResponseEntity.status(400).body(beanError);
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<?> handleException(HttpMessageNotReadableException exception) {
		String fieldName=exception.getCause().getMessage();
		BeanError beanError = new BeanError();
		beanError.setStatus(HttpStatus.BAD_REQUEST);
		Map<String, String> errors = beanError.getErrors();
		errors.put(fieldName.substring(fieldName.lastIndexOf("[\"")+2,fieldName.lastIndexOf("\"]")),"please send correct format");
		errors.put("Error Description",exception.getRootCause().getLocalizedMessage());
		return ResponseEntity.status(400).body(beanError);
	}

	@ExceptionHandler(RetailerException.class)
	public ResponseEntity<?> handleException(RetailerException exception) {
		return ResponseEntity.status(exception.getHttpStatus()).body(this.processErrors(exception.getBindingResult(),exception.getHttpStatus()));
	}
	
	@ExceptionHandler(NoAvailabilityException.class)
	public ResponseEntity<Map<String,Object>> handleException(NoAvailabilityException exception) {
		return ResponseEntity.status(exception.getStatus()).body(exception.getResponse());
	}
	
	private BeanError processErrors(BindingResult bindingResult,HttpStatus status) {
		BeanError beanError = new BeanError();
		beanError.setStatus(status);
		Map<String, String> errors = beanError.getErrors();

		bindingResult.getFieldErrors().stream()
				.forEach((error) -> errors.put(error.getField(), error.getDefaultMessage()));

		return beanError;

	}
}

