<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Retail Manager</title>
 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
</head>
<body ng-app="reatailForm" ng-controller="RetailerController">
<form  method="POST">
<div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">Retailer Details Form</div>
        <div class="panel-body">
          <div class="col-md-12">
              <table class="table table-noborder">
                <tbody>
                  <td><div class="form-group">
                          <label for="shopName">Shop Name</label>
                          <input type="text" class="form-control" id="shopName" ng-model="user.shopName"  placeholder="shop name" >
                          
                        </div></td>
                      <td><div class="form-group">
                          <label for="category">Category</label>
                          <input type="text" class="form-control" id="category" ng-model="user.category" placeholder="category name">
                           
                        </div></td>
                      <td><div class="form-group">
                          <label for="address">Address</label>
                          <input type="text" class="form-control"  ng-model="user.address" placeholder="Enter address" onFocus="initializeAutocomplete()" id="locality" >
                        </div></td>
                      <td><div class="form-group">
                         <label for="proprietorName">Owner Name</label>
                          <input type="text" class="form-control" id="proprietorName" ng-model="user.proprietorName" placeholder="proprietor name" >
                        </div></td>
					</tr>
                </tbody>
              </table>
			  <div align="right">
			  <input type="submit" ng-click="save(user)" value="Save" />
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div>
<br></br>

  </div>
  </form>
  </body>
  </html>
  
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpscimbAi_faNj1kNTGVm6alw-T3BmT9Q&sensor=false&libraries=places"></script>
  <script>
  var lat;
  var lng;
  angular.module('reatailForm', [])
                .controller('RetailerController', ['$scope', '$http', function($scope, $http) {
                	
               	 $scope.url="/create";
               	var config='contenttype';
               	
               	$scope.user = {
               			latitude:lat,
               			longitude:lng
               		}
                
            $scope.save = function(user) {
            	 $http.post($scope.url, JSON.stringify(user)).then(function(response) {
            	      $scope.userlist = response.data;
            	      
            	 });
            }
    }]);
  function initializeAutocomplete(){
    var input = document.getElementById('locality');
    var options = {}
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
       lat = place.geometry.location.lat();
       lng = place.geometry.location.lng();
      var placeId = place.place_id;
      // to set city name, using the locality param
      var componentForm = {
        locality: 'short_name',
      };
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          var val = place.address_components[i][componentForm[addressType]];
          document.getElementById("city").value = val;
        }
      }
  });
  }
 
</script>